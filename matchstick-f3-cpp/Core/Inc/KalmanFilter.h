/*
 * KalmanFilter.h
 *
 * Quaternion based Extended Kalman Filter implementation
 *
 *  Created on: 18 Oct 2020
 *      Author: maciek
 */

#ifndef SRC_KALMANFILTER_H_
#define SRC_KALMANFILTER_H_

#include <Eigen/Dense>

#define PI 							3.1415926535897932384626433832795F
#define DEG_TO_RAD 					0.017453292519943295769236907684886F

typedef Eigen::Matrix<float, 7, 1> Vector7f;
typedef Eigen::Matrix<float, 6, 1> Vector6f;
typedef Eigen::Vector4f Vector4f;
typedef Eigen::Vector3f Vector3f;
typedef Eigen::Matrix3f Matrix3f;
typedef Eigen::Matrix4f Matrix4f;
typedef Eigen::Matrix<float, 7, 7> Matrix7f;
typedef Eigen::Matrix<float, 7, 6> Matrix7_6f;
typedef Eigen::Matrix<float, 7, 3> Matrix7_3f;
typedef Eigen::Matrix<float, 6, 6> Matrix6f;
typedef Eigen::Matrix<float, 6, 7> Matrix6_7f;
typedef Eigen::Matrix<float, 4, 3> Matrix4_3f;
typedef Eigen::Matrix<float, 3, 4> Matrix3_4f;

class KalmanFilter
{
	Vector7f xHat;
	Vector7f xHatBar;
	Vector7f xHatPrev;
	Vector6f yHatBar;
	Matrix7f p;
	Matrix7f pBar;
	Matrix7f Q;  						// process variance
	Matrix6f R;  						// measurement variance
	Matrix7_6f K;
	Matrix7f A;
	Matrix7_3f B;
	Matrix6_7f C;
	Vector3f accRef;
	Vector3f magRef;

	Matrix3f mag_A_inv;
	Vector3f mag_b;

	Matrix3f getRotMx(Vector7f quaterion);

	Vector7f normaliseQuat(Vector7f quat);

	Matrix3_4f getJacobian(Vector3f ref);
	Vector6f predictAccMag();

	float rad2deg(float rad);
	float invSqrt(float x);
	Matrix6f invert6f(Matrix6f in);

public:
	KalmanFilter();
	volatile float yaw, pitch, roll;

	void predict(float w1, float w2, float w3, float dt);
	void update(float a1, float a2, float a3, float m1, float m2, float m3);
	void getEuler();
};

extern KalmanFilter EKF;

#endif /* SRC_KALMANFILTER_H_ */
