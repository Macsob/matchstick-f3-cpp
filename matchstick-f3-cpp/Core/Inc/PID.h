/*
 * PID.h
 *
 *  Created on: 29 Oct 2020
 *      Author: maciek
 */

#ifndef SRC_PID_H_
#define SRC_PID_H_

#define D_TERM_GAIN 0.95f

class PID {
private:
	float _G = 0;
	float _Kp, _Kd, _Ki = 0;
	float _error, _errorPrev = 0;
	float _integrator = 0;
	float _dTermPrev = 0;

public:
	PID();
	void setGains(const float gains[4]);
	float computeCorrection(float setpoint, float position, float dt);
	void reset();
};

#endif /* SRC_PID_H_ */
