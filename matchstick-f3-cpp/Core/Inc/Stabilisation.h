/*
 * Stabilisation.h
 *
 *  Created on: 29 Oct 2020
 *      Author: maciek
 */

#ifndef INC_STABILISATION_H_
#define INC_STABILISATION_H_

#include "PID.h"
#include "Motors.h"
#include "FlySkyIbus.h"
#include "Attitude.h"
#include "KalmanFilter.h"

#define constrain(amt, low, high) ((amt) < (low) ? (low) : ((amt) > (high) ? (high) : (amt)))

#define ROLL_CHANNEL			0
#define PITCH_CHANNEL			1
#define THROTTLE_CHANNEL		VRA_CHANNEL
#define YAW_CHANNEL				3
#define SWD_CHANNEL				4
#define ARM_CHANNEL				5
#define SWA_CHANNEL				6
#define STAB_CHANNEL			7
#define VRB_CHANNEL				8
#define VRA_CHANNEL				9
#define PUSHER_CHANNEL			2

#define LEFT_SERVO_OFFSET		0
#define RIGHT_SERVO_OFFSET		0
#define THROT_OFFSET			50

#define SERVO_IDLE				1500
#define MOTOR_IDLE				980

#define VERTICAL_CONST			98	// vertical flight pitch angle

class Stabilisation {
private:
	/* Vertical rate gains */
	const float yawRateVertGains[4] = {0.01, 100, 0.1, 0.5};
	const float pitchRateVertGains[4] = {0.01, 150, 0.5, 0.5};
	const float rollRateVertGains[4] = {0.01, 100, 0.5, 0.5};

	/* Vertical angle gains */
	const float rollAngleVertGains[4] = {0.01, 500, 60, 2};
	const float pitchAngleVertGains[4] = {0.01, 320, 62, 0.5};

	/* Horizontal rate gains */
	const float yawRateHorGains[4] = {0.01, 150, 0, 2};
	const float pitchRateHorGains[4] = {0.01, 150, 1, 2};
	const float rollRateHorGains[4] = {0.01, 150, 1, 2};

	/* Horizontal angle gains */
	const float rollAngleHorGains[4] = {0.01, 1550, 195, 0.1};
	const float pitchAngleHorGains[4] = {0.01, 2000, 180, 0.03};

	PID yawRateVert, pitchRateVert, rollRateVert;
	PID pitchAngleVert, rollAngleVert;

	PID yawRateHor, pitchRateHor, rollRateHor;
	PID pitchAngleHor, rollAngleHor;

	FlySkyIbus *_ibus;
	Attitude *_sensors;
	KalmanFilter *_ekf;

	Motors ServoR, ServoL, MotorL, MotorR, Pusher;

	float setYawRate, setPitchRate, setRollRate;
	float setPitchAngle, setRollAngle;
	float setThrottle, setPusher, _loopTimeSec;

	int map(int x, int in_min, int in_max, int out_min, int out_max);
	float mapFloat(int x, int in_min, int in_max, int out_min, int out_max);
	int16_t reverse(int16_t val);
	int16_t add(int16_t a, int16_t b);



public:
	Stabilisation(FlySkyIbus * ibus, Attitude * sensors, KalmanFilter * ekf);
	void init(float loopTimeSec);
	void idle();
	void acroVert();
	void angleVert();
	void acroHor();
	void angleHor();
	void safeDescend();
	void safeGlide();
	void getSetpoints();
	void getFlightMode();
};

extern Stabilisation Pilot;

#endif /* INC_STABILISATION_H_ */
