/*
 * RingBuffer.h
 *
 *  Created on: 16 Oct 2020
 *      Author: maciek
 */

#ifndef SRC_RINGBUFFER_H_
#define SRC_RINGBUFFER_H_

#include "stm32f3xx_hal.h"

#define UART_BUFFER_SIZE 512

class RingBuffer {
	UART_HandleTypeDef * _uart;
	unsigned char buffer[UART_BUFFER_SIZE];
	volatile unsigned int head;
	volatile unsigned int tail;
	void storeChar(unsigned char c);
	void put(char c);
public:
	RingBuffer(UART_HandleTypeDef* uart);
	void initRx();
	void sendString(char* s);
	int get();
	int IsDataAvailable();
	void Uart_isr_rx(UART_HandleTypeDef *huart); // put in IRQ handler
	void Uart_isr_tx(UART_HandleTypeDef *huart); // put in IRQ handler
	char sprintf_buf[100];
};

extern RingBuffer Uart1_rx_buff;
extern RingBuffer Uart2_tx_buff;

#endif /* SRC_RINGBUFFER_H_ */
