/*
 * Led.h
 *
 *  Created on: 16 Oct 2020
 *      Author: maciek
 */

#ifndef SRC_LED_H_
#define SRC_LED_H_

#include "stm32f3xx_hal.h"
#include "gpio.h"

class Led {
private:
	GPIO_TypeDef* _port;											// GPIOA, GPIOB, ...
	uint16_t _pin;													// GPIO_PIN_0 ...
	void on(){HAL_GPIO_WritePin(_port, _pin, GPIO_PIN_SET);}		// turn on LED
	void off(){HAL_GPIO_WritePin(_port, _pin, GPIO_PIN_RESET);}		// turn off LED
	void toggle(){HAL_GPIO_TogglePin(_port, _pin);}					// toggle LED
public:
	Led(GPIO_TypeDef* port,uint16_t pin);
	void toggleFor(uint16_t toggleTime);
	void toggleLed();
	void turnOn();
	void turnOff();
};

extern Led led1;
extern Led led2;
extern Led led3;

#endif /* SRC_LED_H_ */
