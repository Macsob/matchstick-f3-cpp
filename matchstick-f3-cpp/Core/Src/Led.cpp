/*
 * Led.cpp
 *
 *  Created on: 16 Oct 2020
 *      Author: maciek
 */

#include "Led.h"

Led led1(GPIO_OUT_LED1_GPIO_Port, GPIO_OUT_LED1_Pin);
Led led2(GPIO_OUT_LED2_GPIO_Port, GPIO_OUT_LED2_Pin);
Led led3(GPIO_OUT_LED3_GPIO_Port, GPIO_OUT_LED3_Pin);

Led::Led(GPIO_TypeDef* port, uint16_t pin)
{
  _port=port;
  _pin=pin;
  off();
}

void Led::toggleLed(){
	toggle();
}

void Led::toggleFor(uint16_t toggleTime)
{
	on();
	HAL_Delay(toggleTime);
	off();
	HAL_Delay(toggleTime);
}

void Led::turnOn()
{
	on();
}

void Led::turnOff()
{
	off();
}
