/*
 * FlySkyIbus.cpp
 *
 *  Created on: 17 Oct 2020
 *      Author: maciek
 */

#include "FlySkyIbus.h"

FlySkyIbus IBus(Uart1_rx_buff);

FlySkyIbus::FlySkyIbus(RingBuffer& stream) {
	this->stream = &stream;
	this->state = DISCARD;
	this->last = HAL_GetTick();
	this->ptr = 0;
	this->len = 0;
	this->chksum = 0;
	this->lchksum = 0;
}

void FlySkyIbus::loop(void)
{
  while (stream->IsDataAvailable() > 0)
  {
    uint32_t now = HAL_GetTick();
    if (now - last >= PROTOCOL_TIMEGAP)
    {
      state = GET_LENGTH;
    }
    last = now;

    uint8_t v = stream->get();
    switch (state)
    {
      case GET_LENGTH:
        if (v <= PROTOCOL_LENGTH)
        {
          ptr = 0;
          len = v - PROTOCOL_OVERHEAD;
          chksum = 0xFFFF - v;
          state = GET_DATA;
        }
        else
        {
          state = DISCARD;
        }
        break;

      case GET_DATA:
        buffer[ptr++] = v;
        chksum -= v;
        if (ptr == len)
        {
          state = GET_CHKSUML;
        }
        break;

      case GET_CHKSUML:
        lchksum = v;
        state = GET_CHKSUMH;
        break;

      case GET_CHKSUMH:
        // Validate checksum
        if (chksum == (v << 8) + lchksum)
        {
          // Execute command - we only know command 0x40
          switch (buffer[0])
          {
            case PROTOCOL_COMMAND40:
              // Valid - extract channel data
              for (uint8_t i = 1; i < PROTOCOL_CHANNELS * 2 + 1; i += 2)
              {
                channel[i / 2] = buffer[i] | (buffer[i + 1] << 8);
              }
              break;

            default:
              break;
          }
        }
        state = DISCARD;
        break;

      case DISCARD:
      default:
        break;
    }
  }
}

uint16_t FlySkyIbus::readChannel(uint8_t channelNr)
{
  if (channelNr < PROTOCOL_CHANNELS)
  {
    return channel[channelNr];
  }
  else
  {
    return 0;
  }
}
