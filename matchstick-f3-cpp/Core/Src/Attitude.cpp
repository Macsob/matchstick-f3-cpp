/*
 * Attitude.cpp
 *
 *  Created on: 17 Oct 2020
 *      Author: maciek
 */

#include "Attitude.h"
#include "Led.h"

extern I2C_HandleTypeDef hi2c1;

Attitude Sensors(&hi2c1);

Attitude::Attitude(I2C_HandleTypeDef *i2c) : gyroOffsets{0, 0, 0}
{
	_i2c = i2c;

}

/** Write multiple bits in an 8-bit device register.
 * @param dev_addr 	I2C slave device address
 * @param reg_addr 	Register regAddr to write to
 * @param start_bit First bit position to write (0-7)
 * @param len 		Number of bits to write (not more than 8)
 * @param data 		Right-aligned value to write
 * @return Status of operation (0 = success, <0 = error)
 */
HAL_StatusTypeDef Attitude::writeBits(uint8_t dev_addr, uint8_t reg_addr, uint8_t start_bit, uint8_t len, uint8_t data) {
/*     010 value to write
  76543210 bit numbers
     xxx   args: bitStart=4, length=3
  00011100 mask byte
  10101111 original value (sample)
  10100011 original & ~mask
  10101011 masked | value */

  uint8_t b;
  HAL_StatusTypeDef err;

  if ((err = HAL_I2C_Mem_Read(_i2c, dev_addr, reg_addr, 1, &b, 1, 1000)) == 0) {
    uint8_t mask = ((1 << len) - 1) << (start_bit - len + 1);
    data <<= (start_bit - len + 1);  // shift data into correct position
    data &= mask;                    // zero all non-important bits in data
    b &= ~(mask);                    // zero all important bits in existing byte
    b |= data;                       // combine data with existing byte

    return HAL_I2C_Mem_Write(_i2c, dev_addr, reg_addr, 1, &b, 1, 1000);
  } else {
    return err;
  }
}

void Attitude::readSlaveRegisters(uint8_t AuxDevAddr, uint8_t AuxMemAddr, uint8_t* buf, uint32_t count, uint32_t timeout) {
  uint8_t ctrl = I2C_READ_FLAG | count;
  uint8_t devAddr = I2C_READ_FLAG | AuxDevAddr;

  HAL_I2C_Mem_Write(_i2c, MPU6050_ADDR, MPU6050_RA_I2C_SLV0_ADDR, I2C_MEMADD_SIZE_8BIT, &devAddr, 1, timeout);
  HAL_I2C_Mem_Write(_i2c, MPU6050_ADDR, MPU6050_RA_I2C_SLV0_REG, I2C_MEMADD_SIZE_8BIT, &AuxMemAddr, 1, timeout);
  HAL_I2C_Mem_Write(_i2c, MPU6050_ADDR, MPU6050_RA_I2C_SLV0_CTRL, I2C_MEMADD_SIZE_8BIT, &ctrl, 1, timeout);

//  HAL_Delay(1); // wait for registers fill

  HAL_I2C_Mem_Read(&hi2c1, MPU6050_ADDR, MPU6050_RA_EXT_SENS_DATA_00, I2C_MEMADD_SIZE_8BIT, buf, count, timeout);
}

void Attitude::writeSlaveRegister(uint8_t AuxDevAddr, uint8_t AuxMemAddr, uint8_t* data, uint32_t timeout) {
  uint8_t ctrl = 0x81;

  HAL_I2C_Mem_Write(_i2c, MPU6050_ADDR, MPU6050_RA_I2C_SLV0_ADDR, 1, &AuxDevAddr, 1, timeout);
  HAL_I2C_Mem_Write(_i2c, MPU6050_ADDR, MPU6050_RA_I2C_SLV0_REG, 1, &AuxMemAddr, 1, timeout);
  HAL_I2C_Mem_Write(_i2c, MPU6050_ADDR, MPU6050_RA_I2C_SLV0_DO, 1, data, 1, timeout);
  HAL_I2C_Mem_Write(_i2c, MPU6050_ADDR, MPU6050_RA_I2C_SLV0_CTRL, 1, &ctrl, 1, timeout);

}

void Attitude::MPU6050_Init() {
  uint8_t check;
  uint8_t Data;

  HAL_I2C_Mem_Read(_i2c, MPU6050_ADDR, WHO_AM_I_REG, 1, &check, 1, 1000);  // check device ID

  if (check == 104) {  // if the device is present

	// reset device
	Data = 0x80;
	HAL_I2C_Mem_Write(_i2c, MPU6050_ADDR, POWER_MANAGEMENT_REG, 1, &Data, 1, 1000);

	HAL_Delay(2);

    // to wake up the sensor write 0's to power management register 0x6B
    Data = 0;
    HAL_I2C_Mem_Write(_i2c, MPU6050_ADDR, POWER_MANAGEMENT_REG, 1, &Data, 1, 1000);

    // set clock source
    writeBits(MPU6050_ADDR, MPU6050_RA_PWR_MGMT_1, MPU6050_PWR1_CLKSEL_BIT, MPU6050_PWR1_CLKSEL_LENGTH, MPU6050_CLOCK_PLL_XGYRO);

    // to set data rate to 1KHz
    Data = 0x07;
    HAL_I2C_Mem_Write(_i2c, MPU6050_ADDR, SMPLRT_DIV_REG, 1, &Data, 1, 1000);

    // set acc configuration
    Data = 0x02;
    writeBits(MPU6050_ADDR, ACCEL_CONFIG_REG, MPU6050_ACONFIG_AFS_SEL_BIT, MPU6050_ACONFIG_AFS_SEL_LENGTH, Data);

    // set gyro configuration
    Data = 0x02;
    writeBits(MPU6050_ADDR, GYRO_CONFIG_REG, MPU6050_GCONFIG_FS_SEL_BIT, MPU6050_GCONFIG_FS_SEL_LENGTH, Data);

    // enable low pass filter with cut off at 42Hz
    writeBits(MPU6050_ADDR, MPU6050_RA_CONFIG, MPU6050_CFG_DLPF_CFG_BIT, MPU6050_CFG_DLPF_CFG_LENGTH, MPU6050_DLPF_BW_20);

    // bypass enable so that HMC5883 can communicate, auxiliary I2C pass-through, 0x02 for enable
    Data = 0x02;
    HAL_I2C_Mem_Write(_i2c, MPU6050_ADDR, MPU6050_RA_INT_PIN_CFG, 1, &Data, 1, 1000);

    // set master I2C speed
//    writeBits(MPU6050_ADDR, MPU6050_RA_I2C_MST_CTRL, MPU6050_I2C_MST_CLK_BIT, MPU6050_I2C_MST_CLK_LENGTH, 0x0d);

    // enable I2C master mode (write 1 in MST_EN_BIT)
//    writeBits(MPU6050_ADDR, MPU6050_RA_USER_CTRL, MPU6050_USERCTRL_I2C_MST_EN_BIT, 1, 1);

    /* set up slave magnetometer

     // Write CONFIG_A register
       Data = (HMC5883L_AVERAGING_8 << (HMC5883L_CRA_AVERAGE_BIT - HMC5883L_CRA_AVERAGE_LENGTH + 1)) |
              (HMC5883L_RATE_15 << (HMC5883L_CRA_RATE_BIT - HMC5883L_CRA_RATE_LENGTH + 1)) |
              (HMC5883L_BIAS_NORMAL << (HMC5883L_CRA_BIAS_BIT - HMC5883L_CRA_BIAS_LENGTH + 1));
       writeSlaveRegister(HMC5883L_ADDRESS_NO_SHIFT, HMC5883L_RA_CONFIG_A, &Data, 1000);

       // write CONFIG_B register
       Data = HMC5883L_GAIN_1090 << (HMC5883L_CRB_GAIN_BIT - HMC5883L_CRB_GAIN_LENGTH + 1);
       writeSlaveRegister(HMC5883L_ADDRESS_WRITE, HMC5883L_RA_CONFIG_B, &Data, 1000);

       // write MODE register
       Data = HMC5883L_MODE_CONTINUOUS << (HMC5883L_MODEREG_BIT - HMC5883L_MODEREG_LENGTH + 1);
       writeSlaveRegister(HMC5883L_ADDRESS_WRITE, HMC5883L_RA_MODE, &Data, 1000);

       // last read will be repeated at data transfer rate
       readSlaveRegisters(HMC5883L_ADDRESS_READ, HMC5883L_RA_DATAX_H, &AHRS_buffer_DMA[14], 6, 1000);

	*/

  } else {
    HAL_Delay(20);
    MPU6050_Init();
  }
}

void Attitude::HMC5883L_Init() {
  uint8_t check;
  uint8_t Data;

  // check if Identification Register A is == 0x48
  HAL_I2C_Mem_Read(_i2c, HMC5883L_ADDRESS, HMC5883L_RA_ID_A, I2C_MEMADD_SIZE_8BIT, &check, 1, 1000);

  if (check == 0x48) {
    // write CONFIG_A register
    Data = (HMC5883L_AVERAGING_8 << (HMC5883L_CRA_AVERAGE_BIT - HMC5883L_CRA_AVERAGE_LENGTH + 1)) |
           (HMC5883L_RATE_15 << (HMC5883L_CRA_RATE_BIT - HMC5883L_CRA_RATE_LENGTH + 1)) |
           (HMC5883L_BIAS_NORMAL << (HMC5883L_CRA_BIAS_BIT - HMC5883L_CRA_BIAS_LENGTH + 1));

    HAL_I2C_Mem_Write(_i2c, HMC5883L_ADDRESS, HMC5883L_RA_CONFIG_A, 1, &Data, 1, 1000);

    // write CONFIG_B register
    Data = HMC5883L_GAIN_1090 << (HMC5883L_CRB_GAIN_BIT - HMC5883L_CRB_GAIN_LENGTH + 1);

    HAL_I2C_Mem_Write(_i2c, HMC5883L_ADDRESS, HMC5883L_RA_CONFIG_B, 1, &Data, 1, 1000);

    // write MODE register
    Data = HMC5883L_MODE_CONTINUOUS << (HMC5883L_MODEREG_BIT - HMC5883L_MODEREG_LENGTH + 1);

    HAL_I2C_Mem_Write(_i2c, HMC5883L_ADDRESS, HMC5883L_RA_MODE, 1, &Data, 1, 1000);
  } else {
	HAL_Delay(20);
    HMC5883L_Init();
  }
}

void Attitude::getMotion9() {

    /* Wait for the end of the transfer + timeout! or callbacks and flags!*/
    while (HAL_I2C_GetState(_i2c) != HAL_I2C_STATE_READY)
    {
    }

    /* poll for mag data, both imu and mag don't work with DMA at the same time, need to reconfigure MPU6050 master I2C */
    HAL_I2C_Mem_Read(&hi2c1, HMC5883L_ADDRESS, HMC5883L_RA_DATAX_H, I2C_MEMADD_SIZE_8BIT, HMC_buffer, 6, 10);

    Accel_X_Raw = (((int16_t)MPU_buffer[0]) << 8) | MPU_buffer[1];
    Accel_Y_Raw = (((int16_t)MPU_buffer[2]) << 8) | MPU_buffer[3];
    Accel_Z_Raw = (((int16_t)MPU_buffer[4]) << 8) | MPU_buffer[5];

    Gyro_X_Raw = (((int16_t)MPU_buffer[8]) << 8) | MPU_buffer[9];
    Gyro_Y_Raw = (((int16_t)MPU_buffer[10]) << 8) | MPU_buffer[11];
    Gyro_Z_Raw = (((int16_t)MPU_buffer[12]) << 8) | MPU_buffer[13];

    Mag_X_Raw = (int16_t)(HMC_buffer[0] << 8 | HMC_buffer[1]);
    Mag_Z_Raw = (int16_t)(HMC_buffer[2] << 8 | HMC_buffer[3]);
    Mag_Y_Raw = (int16_t)(HMC_buffer[4] << 8 | HMC_buffer[5]);

    Ax = Accel_X_Raw / 4096.0f;
    Ay = Accel_Y_Raw / 4096.0f;
    Az = Accel_Z_Raw / 4096.0f;

    Gx = (Gyro_X_Raw - gyroOffsets[0]) / 32.8f;
    Gy = (Gyro_Y_Raw - gyroOffsets[1]) / 32.8f;
    Gz = (Gyro_Z_Raw - gyroOffsets[2]) / 32.8f;

    Mx = Mag_X_Raw * 0.092f;
    My = Mag_Y_Raw * 0.092f;
    Mz = Mag_Z_Raw * 0.092f;

    /* Tell DMA to read raw values whenever there is some free time */
    if(HAL_I2C_Mem_Read_DMA(_i2c, MPU6050_ADDR, ACCEL_XOUT_H_REG, I2C_MEMADD_SIZE_8BIT, MPU_buffer, 14) != HAL_OK) {
    	/* Reading process Error */
    	// Error_Handler();
    }
}

void Attitude::computeMean(int16_t sampleList[], int size, float *mean) {
  (*mean) = 0;
  for (int sample = 0; sample < size; sample++) {
    (*mean) = (*mean) + sampleList[sample];
  }
  (*mean) = (*mean) / size;
}

int Attitude::computeOffsets() {
  int16_t gyroRAW[AXIS_NUM][SAMPLE_NUM];
  float mean = 0;

  for (int axis = 0; axis < AXIS_NUM; axis++)
    for (int sample = 0; sample < SAMPLE_NUM; sample++) gyroRAW[axis][sample] = 0;

  // get samples
  for (int sample = 0; sample < SAMPLE_NUM; sample++) {
    getMotion9();
    gyroRAW[0][sample] = Gyro_X_Raw;
    gyroRAW[1][sample] = Gyro_Y_Raw;
    gyroRAW[2][sample] = Gyro_Z_Raw;
    HAL_Delay(200);
    led3.toggleLed();
  }

  // compute mean
  for (int axis = 0; axis < AXIS_NUM; axis++) {
    computeMean(gyroRAW[axis], SAMPLE_NUM, &mean);
    gyroOffsets[axis] = (int16_t)(mean);
  }

  if (mean != 0) return 1;
  else
  {
    return -1;
  }
}
