/*
 * KalmanFilter.cpp
 *
 * Quaternion based Extended Kalman Filter implementation
 *
 *  Created on: 18 Oct 2020
 *      Author: maciek
 */

#include "KalmanFilter.h"
#include <cmath>

KalmanFilter EKF;

KalmanFilter::KalmanFilter()
{
	mag_A_inv 	<<	1.95518283e-02, 5.88907447e-05, 1.89091297e-04,
					5.88907447e-05, 1.98932356e-02, 4.12818514e-05,
					1.89091297e-04, 4.12818514e-05, 2.07221979e-02;

	mag_b		<< 	10.3316632,
					3.82743126,
					-3.47550067;

	xHat		<< 	1,
					0,
					0,
					0,
					0,
					0,
					0;

	p = Matrix7f::Identity(7, 7) * 0.1f;		// smaller p takes longer to converge, default 0.1f
	Q = Matrix7f::Identity(7, 7) * 0.01f;		// process noise convariance, larger is more responsive, default 0.001f
	R = Matrix6f::Identity(6, 6) * 100.0f;		// measurement noise covariance default 100.0f

	accRef		<<	0,
					0,
					1;

	magRef 		<<	0,
				   -1,
					0;

}

/* Fast inverse square-root
 * See: http://en.wikipedia.org/wiki/Fast_inverse_square_root
 */
float KalmanFilter::invSqrt(float x) {
float xhalf = 0.5f*x;
  uint32_t i;
  assert(sizeof(x) == sizeof(i));
  memcpy(&i, &x, sizeof(i));
  i = 0x5f375a86 - (i>>1);
  memcpy(&x, &i, sizeof(i));
  x = x*(1.5f - xhalf*x*x);
  return x;
}

float KalmanFilter::rad2deg(float rad) {
  return rad / PI * 180;
}

Matrix3f KalmanFilter::getRotMx(Vector7f quaterion) {
	Matrix3f rotMx;

	rotMx(0, 0) = pow(quaterion(0, 0), 2) + pow(quaterion(1, 0), 2) - pow(quaterion(2, 0), 2) - pow(quaterion(3, 0), 2);
	rotMx(0, 1) = 2 * (quaterion(1, 0) * quaterion(2, 0) - quaterion(0, 0) * quaterion(3, 0));
	rotMx(0, 2) = 2 * (quaterion(1, 0) * quaterion(3, 0) + quaterion(0, 0) * quaterion(2, 0));
	rotMx(1, 0) = 2 * (quaterion(1, 0) * quaterion(2, 0) + quaterion(0, 0) * quaterion(3, 0));
	rotMx(1, 1) = pow(quaterion(0, 0), 2) - pow(quaterion(1, 0), 2) + pow(quaterion(2, 0), 2) - pow(quaterion(3, 0), 2);
	rotMx(1, 2) = 2 * (quaterion(2, 0) * quaterion(3, 0) - quaterion(0, 0) * quaterion(1, 0));
	rotMx(2, 0) = 2 * (quaterion(1, 0) * quaterion(3, 0) - quaterion(0, 0) * quaterion(2, 0));
	rotMx(2, 1) = 2 * (quaterion(2, 0) * quaterion(3, 0) + quaterion(0, 0) * quaterion(1, 0));
	rotMx(2, 2) = pow(quaterion(0, 0), 2) - pow(quaterion(1, 0), 2) - pow(quaterion(2, 0), 2) + pow(quaterion(3, 0), 2);

	return rotMx;
}

void KalmanFilter::getEuler() {
	Matrix3f rotMx = getRotMx(xHat);
	float test = -rotMx(2, 0);
	float yawRad, pitchRad, rollRad;

	if (test > 0.99999f) {
		yawRad = 0;
		pitchRad = PI / 2;
		rollRad = atan2(rotMx(0, 1), rotMx(2, 0));
	} else if (test < -0.99999f) {
		yawRad = 0;
		pitchRad = -PI / 2;
		rollRad = atan2(-rotMx(0, 1), -rotMx(0, 2));
	} else {
		yawRad = atan2(rotMx(1, 0), rotMx(0, 0));
		pitchRad = asin(-rotMx(2, 0));
		rollRad = atan2(rotMx(2, 1), rotMx(2, 2));
	}

  /* swap axis here */
  yaw = rad2deg(yawRad);
  pitch = rad2deg(rollRad);
  roll = rad2deg(pitchRad);
}

Vector7f KalmanFilter::normaliseQuat(Vector7f quat) {
  float mag = invSqrt(pow(quat(0, 0), 2) + pow(quat(1, 0), 2) + pow(quat(2, 0), 2) + pow(quat(3, 0), 2));

  for (int i = 0; i < 4; i++) {
    quat(i, 0) *= mag;
  }

  return quat;
}

Matrix3_4f KalmanFilter::getJacobian(Vector3f ref) {
  Matrix3_4f j;

  j(0, 0) =  xHatPrev(0, 0) * ref(0, 0) + xHatPrev(3, 0) * ref(1, 0) - xHatPrev(2, 0) * ref(2, 0);
  j(0, 1) =  xHatPrev(1, 0) * ref(0, 0) + xHatPrev(2, 0) * ref(1, 0) + xHatPrev(3, 0) * ref(2, 0);
  j(0, 2) = -xHatPrev(2, 0) * ref(0, 0) + xHatPrev(1, 0) * ref(1, 0) - xHatPrev(0, 0) * ref(2, 0);
  j(0, 3) = -xHatPrev(3, 0) * ref(0, 0) + xHatPrev(0, 0) * ref(1, 0) + xHatPrev(1, 0) * ref(2, 0);
  j(1, 0) = -xHatPrev(3, 0) * ref(0, 0) + xHatPrev(0, 0) * ref(1, 0) + xHatPrev(1, 0) * ref(2, 0);
  j(1, 1) =  xHatPrev(2, 0) * ref(0, 0) - xHatPrev(1, 0) * ref(1, 0) + xHatPrev(0, 0) * ref(2, 0);
  j(1, 2) =  xHatPrev(1, 0) * ref(0, 0) + xHatPrev(2, 0) * ref(1, 0) + xHatPrev(3, 0) * ref(2, 0);
  j(1, 3) = -xHatPrev(0, 0) * ref(0, 0) - xHatPrev(3, 0) * ref(1, 0) + xHatPrev(2, 0) * ref(2, 0);
  j(2, 0) =  xHatPrev(2, 0) * ref(0, 0) - xHatPrev(1, 0) * ref(1, 0) + xHatPrev(0, 0) * ref(2, 0);
  j(2, 1) =  xHatPrev(3, 0) * ref(0, 0) - xHatPrev(0, 0) * ref(1, 0) - xHatPrev(1, 0) * ref(2, 0);
  j(2, 2) =  xHatPrev(0, 0) * ref(0, 0) + xHatPrev(3, 0) * ref(1, 0) - xHatPrev(2, 0) * ref(2, 0);
  j(2, 3) =  xHatPrev(1, 0) * ref(0, 0) + xHatPrev(2, 0) * ref(1, 0) + xHatPrev(3, 0) * ref(2, 0);

  return j * 2;
}

Vector6f KalmanFilter::predictAccMag() {
	Vector6f accMag;

	/* Acc */
	Matrix3_4f hPrime_a = getJacobian(accRef);
	Vector3f accBar = getRotMx(xHatBar).transpose() * accRef;

	/* Mag */
	Matrix3_4f hPrime_m = getJacobian(magRef);
	Vector3f magBar = getRotMx(xHatBar).transpose() * magRef;

	/* C */
	Eigen::Matrix<float, 3, 7> temp1;
	temp1 << hPrime_a, Matrix3f::Zero(3, 3);

	Eigen::Matrix<float, 3, 7> temp2;
	temp2 << hPrime_m, Matrix3f::Zero(3, 3);

	C << 	temp1,
			temp2;

	/* AccMag */
	accMag << 	accBar,
				magBar;

	return accMag;
}

/* dt in seconds! */
void KalmanFilter::predict(float w1, float w2, float w3, float dt) {
  Vector3f w;
  w << w1 * DEG_TO_RAD, w2 * DEG_TO_RAD, w3 * DEG_TO_RAD;		// important! has to be rad/s

  Matrix4_3f Sq;
  Sq <<		-xHat(1, 0), -xHat(2, 0), -xHat(3, 0),
		  	 xHat(0, 0), -xHat(3, 0),  xHat(2, 0),
			 xHat(3, 0),  xHat(0, 0), -xHat(1, 0),
			-xHat(2, 0),  xHat(1, 0),  xHat(0, 0);

  Eigen::Matrix<float, 4, 7> temp1;
  temp1 << Matrix4f::Identity(4, 4), (-dt/2) * Sq;

  Eigen::Matrix<float, 3, 7> temp2;
  temp2 << Eigen::Matrix<float, 3, 4>::Zero(3, 4), Matrix3f::Identity(3, 3);

  A << 	temp1,
		temp2;

  B <<	(dt/2) * Sq,
		Matrix3f::Zero(3, 3);

  xHatBar = A * xHat + B * w;
  xHatBar = normaliseQuat(xHatBar);

  xHatPrev = xHat;

  yHatBar = predictAccMag();

  pBar = (A * p) * A.transpose() + Q;

}

void KalmanFilter::update(float a1, float a2, float a3, float m1, float m2, float m3) {
	float accMagnitude = invSqrt(a1*a1 + a2*a2 + a3*a3);

	Vector3f accVector;
	accVector <<	a1*accMagnitude, a2*accMagnitude, a3*accMagnitude;

	Vector3f magVector;
	magVector << m1, m2, m3;

	Vector3f magRaw = mag_A_inv * (magVector - mag_b);
	Vector3f mag_N = getRotMx(xHat) * magRaw;
	mag_N(2, 0) = 0;

	mag_N *= invSqrt(pow(mag_N(0, 0), 2) + pow(mag_N(1, 0), 2));

	Vector3f mag_B = getRotMx(xHat).transpose() * mag_N;

	Vector6f measurement;
	measurement << 	accVector, mag_B;

	// Eigen inverse method takes too much space use Gauss Jordan Elimination instead
	// K = (pBar * C.transpose()) * ((C*pBar) * C.transpose() + R).inverse();

	Matrix6f temp = invert6f((C*pBar) * C.transpose() + R);
	K = (pBar * C.transpose()) * temp;

	xHat = xHatBar + K * (measurement - yHatBar);
	xHat = normaliseQuat(xHat);

	p = (Matrix7f::Identity(7, 7) - K * C) * pBar;

}

/* Gauss Jordan Elimination */
Matrix6f KalmanFilter::invert6f(Matrix6f in) {
  Matrix6f out;
  int size = 6;
  float factor;
  int i, j, k;
  Matrix6f I = Matrix6f::Identity(6, 6);

  /* concatenate identity matrix */
  Eigen::Matrix<float, 6, 12> tmp;
  tmp	<<	in, I;

  /* Applying Gauss Jordan Elimination */
  for (i = 0; i < size; i++) {
    for (j = 0; j < size; j++) {
      if (i != j) {
        factor = tmp(j, i) / tmp(i, i);
        for (k = 0; k < 2 * size; k++) {
          tmp(j, k) = tmp(j, k) - factor * tmp(i, k);
        }
      }
    }
  }

  /* Row Operation to Make Principal Diagonal to 1 */
  for (i = 0; i < size; i++) {
    for (j = size; j < 2 * size; j++) {
      tmp(i, j) = tmp(i, j) / tmp(i, i);
    }
  }

  /* discard identity matrix */
  for (i = 0; i < size; i++) {
    for (j = size; j < 2 * size; j++) {
      out(i, j - size) = tmp(i, j);
    }
  }

  return out;
}
