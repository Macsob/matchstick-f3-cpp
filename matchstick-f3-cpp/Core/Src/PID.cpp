/*
 * PID.cpp
 *
 *  Created on: 29 Oct 2020
 *      Author: maciek
 */

#include "PID.h"

PID::PID() {

}

void PID::setGains(const float gains[4]) {
	_G = gains[0];
	_Kp = gains[1];
	_Kd = gains[2];
	_Ki = gains[3];
}

float PID::computeCorrection(float setpoint, float position, float dt) {
  float correction = 0;
  _error = setpoint - position;

  /* anti windup (on error sign change) */
  if (_errorPrev * _error <= 0) {
    _integrator = 0;
  } else {
    _integrator = _integrator + _error;
  }

  /* D term low pass filter */
  float dTerm = _Kd * ((_error - _errorPrev) / (dt));
  float dFiltered = (1 - D_TERM_GAIN) * dTerm +  D_TERM_GAIN * _dTermPrev;
  _dTermPrev = dTerm;

  /* correction in us */
  correction = _G * (_Kp * _error + dFiltered + _Ki * _integrator);

  _errorPrev = _error;

  return correction;
}

void PID::reset() {
	_errorPrev = 0;
	_integrator = 0;
	_dTermPrev = 0;
}
