/*
 * Stabilisation.cpp
 *
 *  Created on: 29 Oct 2020
 *      Author: maciek
 */

#include "Stabilisation.h"

Stabilisation Pilot(&IBus, &Sensors, &EKF);

Stabilisation::Stabilisation(FlySkyIbus * ibus, Attitude * sensors, KalmanFilter * ekf) :
														ServoR(&htim1, TIM_CHANNEL_4, &TIM1->CCR4),
														ServoL(&htim1, TIM_CHANNEL_1, &TIM1->CCR1),
														MotorL(&htim2, TIM_CHANNEL_2, &TIM2->CCR2),
														MotorR(&htim2, TIM_CHANNEL_1, &TIM2->CCR1),
														Pusher(&htim3, TIM_CHANNEL_4, &TIM3->CCR4){
	_ibus = ibus;
	_sensors = sensors;
	_ekf = ekf;
}

/* Sets PID gains and loop time in seconds, turns motor pwm timers on and sends idle signal */
void Stabilisation::init(float loopTimeSec) {
	_loopTimeSec = loopTimeSec;
	yawRateVert.setGains(yawRateVertGains);
	pitchRateVert.setGains(pitchRateVertGains);
	rollRateVert.setGains(pitchRateVertGains);

	pitchAngleVert.setGains(pitchAngleVertGains);
	rollAngleVert.setGains(rollAngleVertGains);

	yawRateHor.setGains(yawRateHorGains);
	pitchRateHor.setGains(pitchRateHorGains);
	rollRateVert.setGains(rollRateHorGains);

	pitchAngleHor.setGains(pitchAngleHorGains);
	rollAngleHor.setGains(rollAngleHorGains);

	ServoR.init();
	ServoL.init();
	MotorR.init();
	MotorL.init();
	Pusher.init();

	ServoR.setSpeed(SERVO_IDLE);
	ServoL.setSpeed(SERVO_IDLE);
	MotorR.setSpeed(MOTOR_IDLE);
	MotorL.setSpeed(MOTOR_IDLE);
	Pusher.setSpeed(MOTOR_IDLE);
}

void Stabilisation::getFlightMode() {
	switch (_ibus->readChannel(ARM_CHANNEL)) {
	case 1500:
		if (_ibus->readChannel(STAB_CHANNEL) == 2000) angleVert();
		else acroVert();
		break;
	default:
		if (_ibus->readChannel(SWA_CHANNEL) == 2000) Sensors.computeOffsets();
		idle();
	}
}

void Stabilisation::getSetpoints() {
	setYawRate = -mapFloat(_ibus->readChannel(YAW_CHANNEL), 1000, 2000, -135, 135);
	setPitchRate = -mapFloat(_ibus->readChannel(PITCH_CHANNEL), 1000, 2000, -135, 135);
	setRollRate = -mapFloat(_ibus->readChannel(ROLL_CHANNEL), 1000, 2000, -135, 135);

	setPitchAngle = VERTICAL_CONST + mapFloat(reverse(_ibus->readChannel(PITCH_CHANNEL)), 1000, 2000, -20, 20);  // this is inverted
	setRollAngle = mapFloat(_ibus->readChannel(ROLL_CHANNEL), 1000, 2000, -20, 20);

	setThrottle = _ibus->readChannel(THROTTLE_CHANNEL);

	setPusher = _ibus->readChannel(PUSHER_CHANNEL);
}

void Stabilisation::idle() {
	ServoR.setSpeed(SERVO_IDLE);
	ServoL.setSpeed(SERVO_IDLE);
	MotorR.setSpeed(MOTOR_IDLE);
	MotorL.setSpeed(MOTOR_IDLE);
	Pusher.setSpeed(MOTOR_IDLE);
}

/* Stabilisation in vertical acrobatic flight mode
 * PID and mixing
 */
void Stabilisation::acroVert() {
	/* Yaw rate correction */
	int16_t yawRateCorr = (int16_t)yawRateVert.computeCorrection(setYawRate, _sensors->Gy, _loopTimeSec);
	int16_t yawRateCmd = constrain(1500 - yawRateCorr, 1000, 2000);

	/* Pitch rate correction */
	int16_t pitchRateCorr = (int16_t)pitchRateVert.computeCorrection(setPitchRate, _sensors->Gx, _loopTimeSec);
	int16_t pitchRateCmd = constrain(1500 - pitchRateCorr, 1000, 2000);

	/* Roll rate command */
	int16_t rollRateCorr = (int16_t)rollRateVert.computeCorrection(setRollRate, _sensors->Gz, _loopTimeSec);

	/* Motor mixing */
	MotorL.setSpeed(constrain(setThrottle - rollRateCorr, 1000, 2000) - THROT_OFFSET);
	MotorR.setSpeed(constrain(setThrottle + rollRateCorr, 1000, 2000) - THROT_OFFSET);

	/* Servo mixing */
	ServoL.setSpeed(add(reverse(yawRateCmd), reverse(pitchRateCmd)) - LEFT_SERVO_OFFSET);
	ServoR.setSpeed(add(reverse(yawRateCmd), pitchRateCmd) - RIGHT_SERVO_OFFSET);

	Pusher.setSpeed(setPusher);
}

/* Stabilisation in vertical angle flight mode */
void Stabilisation::angleVert() {
  /* yaw rate command */
  int16_t yawRateCorr = (int16_t)yawRateVert.computeCorrection(setYawRate, _sensors->Gy, _loopTimeSec);
  int16_t yawRateCmd = constrain(1500 - yawRateCorr, 1000, 2000);

  /* pitch position command */
  int16_t pitchCorr = (int16_t)pitchAngleVert.computeCorrection(setPitchAngle, _ekf->pitch, _loopTimeSec);
  // correction for rates
  int16_t pitchAngleCmd = constrain(1500 - pitchCorr, 1000, 2000);

  /* roll position command, swapped with yaw gains! */
  int16_t rollAngleCmd = (int16_t)rollAngleVert.computeCorrection(setRollAngle, _ekf->roll, _loopTimeSec);
  // correction for rates

  /* motor mixing*/
  uint16_t speedMotorL = constrain(setThrottle + rollAngleCmd, 1000, 2000) - THROT_OFFSET;
  MotorL.setSpeed(speedMotorL);
  uint16_t speedMotorR = constrain(setThrottle - rollAngleCmd, 1000, 2000) - THROT_OFFSET;
  MotorR.setSpeed(speedMotorR);

  /* servo mixing */
  uint16_t speedServoL = add(reverse(yawRateCmd), reverse(pitchAngleCmd)) - LEFT_SERVO_OFFSET;
  ServoL.setSpeed(speedServoL);
  uint16_t speedServoR = add(reverse(yawRateCmd), pitchAngleCmd) - RIGHT_SERVO_OFFSET;
  ServoR.setSpeed(speedServoR);

  Pusher.setSpeed(setPusher);
}

void Stabilisation::acroHor() {

}

void Stabilisation::angleHor() {

}

void Stabilisation::safeDescend() {

}

/* Map input range to output range */
int Stabilisation::map(int x, int in_min, int in_max, int out_min, int out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

/* Map input range to output range, return float */
float Stabilisation::mapFloat(int x, int in_min, int in_max, int out_min, int out_max) {
  float divident = (x - in_min) * (out_max - out_min);
  return divident / (in_max - in_min) + out_min;			// divident is cast to float for floating point division
}

/* Reverse servo signal */
int16_t Stabilisation::reverse(int16_t val) {
  return (val - 1500) * -1 + 1500;
}
/* Add servo signal */
int16_t Stabilisation::add(int16_t a, int16_t b) {
  int16_t out = (a - 1500 + b - 1500) + 1500;
  return constrain(out, 1000, 2000);
}
